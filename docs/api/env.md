# Variables d'environnements

Les variables d'environnements sont des variables qui sont définies dans le système d'exploitation. 
Elles sont accessibles par toutes les applications qui sont lancées sur ce système. 

Pour accéder à une variable d'environnement, on peut utiliser la librairie standard `os` et afficher les variables 
avec `os.environ`. 

```python
import os

print(os.environ)

```

## Les fichiers .env

Pour des raisons de sécurité, il est préférable de ne pas stocker les variables d'environnement dans le code  mais de 
les stocker dans un fichier `.env` qui sera lu par le code.

Pour cela, on utilise la librairie `python-dotenv` qui permet de lire un fichier `.env` et de stocker les variables
dans un dictionnaire. Le fichier `.env` doit être dans le même répertoire que le fichier python qui l'utilise et ne
doit surtout pas être versionné dans GIT.

Pour installer la librairie `python-dotenv` :

```shell
pip install python-dotenv
```

Documentation de la librairie : [https://pypi.org/project/python-dotenv/](https://pypi.org/project/python-dotenv/)

**Pour utiliser la librairie `python-dotenv` :**


```python
from dotenv import load_dotenv
import os

# On charge le fichier .env dans le dictionnaire os.environ
load_dotenv()

# On affiche les variables d'environnement
print(os.environ)
```

**Exemple de fichier `.env` :**

```dotenv
# Fichier .env
# Ce fichier ne doit pas être versionné dans GIT

# Variables d'environnement
API_KEY=123456789
API_URL=https://api.example.com
```