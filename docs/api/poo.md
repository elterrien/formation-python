# Création d'un utilitaire à l'aide d'une classe

Pour faciliter, les requêtes vers l'API, nous allons créer une classe `SylobApi`. 


## Initialisation de la classe

La classe pourra contenir plusieurs paramètres comme le `username`, `password` ou `base_url` 
pour stocker l'URL de base de l'API.

Les paramètres de la classe seront initialisés dans la méthode `__init__` qui est appelée.

En fonction du type d'authentification, les paramètres pourront être différents.

<details>
  <summary>Solution</summary>

```python
class SylobApi:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.base_url = "https://jsonplaceholder.typicode.com"
```
</details>

## Création de méthodes

Cette classe va contenir des méthodes pour chaque type de requête. 

Exemple : `get_posts()` pour faire une requête GET vers l'URL `/posts`.

### Création de la méthode `get_posts()`

<details>
  <summary>Solution</summary>

```python
import requests

class SylobApi:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.base_url = "https://jsonplaceholder.typicode.com"
    
    def get_posts(self):
        response = requests.get(f"{self.base_url}/posts")
        return response.json()
        
```
</details>


### Création de la méthode `get_post()`

<details>
  <summary>Solution</summary>

```python
import requests

class SylobApi:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.base_url = "https://jsonplaceholder.typicode.com"
    
    def get_posts(self):
        response = requests.get(f"{self.base_url}/posts")
        return response.json()
    
    def get_post(self, post_id):
        response = requests.get(f"{self.base_url}/posts/{post_id}")
        return response.json()
```

</details>


### Création de la méthode `create_post()`

<details>
  <summary>Solution</summary>

```python
import requests

class SylobApi:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.base_url = "https://jsonplaceholder.typicode.com"
    
    def get_posts(self):
        response = requests.get(f"{self.base_url}/posts")
        return response.json()
    
    def get_post(self, post_id):
        response = requests.get(f"{self.base_url}/posts/{post_id}")
        return response.json()
    
    def create_post(self, title, body, user_id):
        response = requests.post(f"{self.base_url}/posts", json={
            "title": title,
            "body": body,
            "userId": user_id
        })
        return response.json()
```

</details>

## Gestion des erreurs

Lorsque la requête vers l'API échoue, une erreur est levée. Il est préférable de gérer cette erreur pour éviter 
l'arrêt du programme.

<details>
  <summary>Solution</summary>

```python
import requests

class SylobApi:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.base_url = "https://jsonplaceholder.typicode.com"
    
    def get_posts(self):
        try:
            response = requests.get(f"{self.base_url}/posts")
            return response.json()
        except requests.exceptions.RequestException as e:
            print(e)
            return None
    
    def get_post(self, post_id):
        try:
            response = requests.get(f"{self.base_url}/posts/{post_id}")
            return response.json()
        except requests.exceptions.RequestException as e:
            print(e)
            return None
    
    def create_post(self, title, body, user_id):
        try:
            response = requests.post(f"{self.base_url}/posts", json={
                "title": title,
                "body": body,
                "userId": user_id
            })
            return response.json()
        except requests.exceptions.RequestException as e:
            print(e)
            return None
```

</details>


<details>
  <summary>Solution optimisée</summary>

```python

import requests

class SylobApi:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.base_url = "https://jsonplaceholder.typicode.com"
    
    def _request_api(self, method, url, **kwargs):
        try:
            response = requests.request(method, url, **kwargs)
            return response.json()
        except requests.exceptions.RequestException as e:
            print(e)
            return None
    
    def get_posts(self):
        return self._request_api("GET", f"{self.base_url}/posts")
    
    def get_post(self, post_id):
        return self._request_api("GET", f"{self.base_url}/posts/{post_id}")
    
    def create_post(self, title, body, user_id):
        return self._request_api("POST", f"{self.base_url}/posts", json={
            "title": title,
            "body": body,
            "userId": user_id
        })
```
</details>



## Typage des paramètres

Les paramètres des méthodes peuvent être typés pour faciliter l'utilisation de la classe. Lorsque qu'un mauvais type 
est passé en paramètre, une erreur est levée dans l'IDE.

<details>
  <summary>Solution</summary>

```python
import requests

class SylobApi:
    def __init__(self, username: str, password: str):
        self.username = username
        self.password = password
        self.base_url = "https://jsonplaceholder.typicode.com"
    
    def get_posts(self):
        response = requests.get(f"{self.base_url}/posts")
        return response.json()
    
    def get_post(self, post_id: int):
        response = requests.get(f"{self.base_url}/posts/{post_id}")
        return response.json()
    
    def create_post(self, title: str, body: str, user_id: int):
        response = requests.post(f"{self.base_url}/posts", json={
            "title": title,
            "body": body,
            "userId": user_id
        })
        return response.json()
```

</details>