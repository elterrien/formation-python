# Découverte du package requests

Requests est un package open source Python qui permet de faire des requêtes HTTP. 
C'est un package très populaire et très utilisé dans les API REST.

Lien vers le site officiel : [https://requests.readthedocs.io/en/master/](https://requests.readthedocs.io/en/master/)


## Installation

Pour installer le package requests, il suffit d'exécuter la commande suivante dans le terminal :

```shell
pip install requests
```


## Les méthodes requests

Les méthodes principales du package requests sont :

* **get** : pour faire une requête GET -> `requests.get(..)`
* **post** : pour faire une requête POST -> `requests.post(..)`
* **put** : pour faire une requête PUT -> `requests.put(..)`
* **delete** : pour faire une requête DELETE -> `requests.delete(..)`
* **patch** : pour faire une requête PATCH -> `requests.patch(..)`


## Les paramètres

Les méthodes requests prennent en paramètre :

* **url** : l'URL de l'API
* **json** : les données à envoyer au serveur (pour les requêtes POST, PUT et PATCH) au format JSON
* **params** : les paramètres de requête (pour les requêtes GET)
* **headers** : les en-têtes de la requête (pour les requêtes GET, POST, PUT et PATCH)

## Quelques exemples

**Requête GET :**

```python
import requests

response = requests.get("https://jsonplaceholder.typicode.com/posts")

# Affichage du code de la réponse
print(response.status_code)
# Affichage du contenu de la réponse
print(response.json())
```

**Requête GET avec paramètres :**

```python
import requests

# Ajout du paramètre params pour envoyer des paramètres de requête
response = requests.get("https://jsonplaceholder.typicode.com/comments", params={
    "postId": 1
})
# Affichage de l'URL de la requête
print(response.url)

```

**Requête POST :**

```python
import requests

# Ajout du paramètre json pour envoyer des données au serveur
response = requests.post("https://jsonplaceholder.typicode.com/posts", json={
    "title": "My first article",
    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing"
})

# Affichage du code de la réponse
print(response.status_code)

# Affichage du contenu de la réponse
print(response.json())
```

## Authentification

**Authentification basique :**

Un tuple (username, password) peut être passé en paramètre `auth` de des méthode `http` pour faire une 
authentification basique.

```python
import requests
response = requests.get("https://jsonplaceholder.typicode.com/posts", 
                        auth=("username", "password"))
```

Il est également possible d'utiliser le paramètre headers avec le champ `Authorization` pour faire une 
authentification basique. Le token d'authentification correspond à l'encodage en base64 du couple `username:password`.


```python
import requests
requests.get("https://jsonplaceholder.typicode.com/posts", 
             headers={"Authorization": "Basic 37bfd28e-2930-3193-9ac4-1faca"})
```

**Authentification Bearer token :**

Pour faire une authentification Bearer token, il suffit d'ajouter le token dans le champ `Authorization` du paramètre headers.

```python
import requests
response = requests.get("https://jsonplaceholder.typicode.com/posts", 
                        headers={"Authorization": "Bearer 37bfd28e-2930-3193-9ac4-1facabe654d8"})
```

## Les exceptions

Les exceptions suivantes peuvent être levées par le package requests :

* **ConnectionError** : erreur de connexion due à un problème réseau
* **Timeout** : délai d'attente d'une réponse est dépassé
* **TooManyRedirects** : trop de redirections ont été effectuées
* **RequestException** : exception générique pour les autres exceptions

Lorsqu'une exception est levée, cela signifie que la requête n'a pas pu aboutir. **Si les exceptions ne sont pas gérées,
le programme s'arrête**.

Exemples de gestion d'exceptions en Python : [https://realpython.com/python-exceptions/](https://realpython.com/python-exceptions/)

### Exemples de gestion d'exception

```python
import requests
try:
    response = requests.get("https://jsonplaceholder.typicode.com/posts")
except requests.exceptions.ConnectionError as e:
    print(e)
except requests.exceptions.RequestException as e:
    print(e)
```
