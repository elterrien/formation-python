# Introduction aux requêtes API


## Qu'est-ce qu'une requête API ?

Une requête API (Application Programming Interface) est une demande envoyée à un serveur web pour récupérer des données 
ou effectuer une action spécifique via une interface de programmation. Les API sont utilisées pour permettre aux 
applications de communiquer entre elles et d'échanger des données de manière structurée.

Les requêtes API peuvent prendre différentes formes selon le type de méthode utilisée pour l'appel, telles que 
GET, POST, PUT ou DELETE, et peuvent être envoyées via différents protocoles de communication tels que HTTP ou HTTPS.

Les réponses renvoyées par les requêtes API peuvent être au format JSON, XML ou d'autres formats spécifiques, 
selon les spécifications de l'API en question. Les développeurs utilisent les requêtes API pour intégrer les 
fonctionnalités d'un service tiers à leurs propres applications, ce qui permet une interopérabilité plus efficace entre 
différentes plateformes logicielles.

``` mermaid
sequenceDiagram
  autonumber
  client ->> server: GET /api/v1/data
  server ->> client: 200 OK
```

1. Le client envoie une requête GET à l'API.
2. L'API renvoie une réponse 200 OK au client.


## Quels sont les différents standards d'API ?

Il existe plusieurs standards d'API couramment utilisés pour les applications modernes, chacun ayant des avantages et 
des inconvénients selon les besoins spécifiques de l'application. Voici trois exemples de standards d'API populaires :

**REST (Representational State Transfer)** : REST est un style d'architecture d'API qui utilise des méthodes HTTP standard 
telles que GET, POST, PUT et DELETE pour échanger des données entre les clients et les serveurs. Les API REST sont 
généralement basées sur des ressources, qui peuvent être des objets, des collections d'objets ou des actions. 
Le protocole REST est le plus couramment utilisé pour les API Web. 

**gRPC** : gRPC est un framework d'API open-source créé par Google qui utilise le protocole RPC (Remote Procedure Call) 
pour communiquer entre les clients et les serveurs. Il permet de communiquer en 
flux continu et de manière bidirectionnelle.

**SOAP (Simple Object Access Protocol)** : SOAP est un protocole basé sur XML pour l'échange de données entre les 
applications. Les API SOAP utilisent des contrats WSDL (Web Services Description Language) pour décrire les services 
disponibles et les opérations possibles. Ce protocole est encore utilisé sur d'anciennes applications, il a été 
délaissé au profit de REST.

D'autres exemples de standards d'API incluent **GraphQL**, **OData**, **JSON-RPC**, **XML-RPC**, etc. Chaque standard 
d'API a des avantages et des inconvénients, et il est important de choisir le standard qui convient le mieux aux 
besoins de votre application.

## Quelles sont les caractéristiques du standard REST ?

Le standard REST est un style d'architecture d'API qui utilise des méthodes HTTP standard telles que :

* **GET** : pour récupérer des données
* **POST** : pour créer des données
* **PUT** : pour mettre à jour des données
* **DELETE** : pour supprimer des données

**Body** : Dans une requête POST, PUT ou PATCH, les données à envoyer au serveur sont généralement incluses dans 
le corps de la requête, qui peut être au format JSON, XML ou d'autres formats spécifiques. Le corps de la 
requête peut contenir des informations sur les ressources à créer, à mettre à jour ou à supprimer.

<u>Exemple :</u> `POST /articles` avec un corps de requête JSON contenant les informations sur l'article à créer.

*exemple de body :*
```json
{
  "title": "My first article",
  "content": "Lorem ipsum dolor sit amet, consectetur adipiscing"
}
```


**Path** : Dans une API REST, les ressources sont identifiées par une URL unique qui est appelée "chemin" ou "path". 
Par exemple, dans une API de blog, le chemin "/articles" pourrait représenter la liste des articles, tandis que le 
chemin "/articles/123" représente un article spécifique identifié par son ID.

<u>Exemple :</u> `GET /articles/123` pour récupérer l'article avec l'ID 123. 

**Query** : Les requêtes peuvent également inclure des paramètres de requête, qui sont des paires clé-valeur ajoutées 
à la fin de l'URL après un point d'interrogation (?). Les paramètres de requête peuvent être utilisés pour filtrer, 
trier ou paginer les résultats d'une requête. 

<u>Exemple :</u> `GET /articles?category=sports` pour récupérer les articles de la catégorie "sports"


## Quelles sont les caractéristiques des réponses API REST ?


### Code de statut HTTP

Chaque réponse API REST renvoie un code de statut HTTP standard pour indiquer si la requête a réussi ou non. Les codes
de statut HTTP les plus courants sont :

* **200 OK** : la requête a réussi
* **201 Created** : la requête a réussi et une nouvelle ressource a été créée
* **400 Bad Request** : la requête a échoué car les données fournies dans le corps de la requête sont invalides
* **401 Unauthorized** : la requête a échoué car l'utilisateur n'est pas authentifié
* **403 Forbidden** : la requête a échoué car l'utilisateur n'a pas les autorisations nécessaires
* **404 Not Found** : la requête a échoué car la ressource demandée n'existe pas
* **500 Internal Server Error** : la requête a échoué car une erreur interne s'est produite sur le serveur

Il existe de nombreux autres codes de statut HTTP, mais les codes de statut ci-dessus sont les plus couramment utilisés.


### Headers

Les réponses API REST peuvent également inclure des en-têtes HTTP, qui fournissent des informations supplémentaires 
commme le type de contenu, la taille du contenu, etc. Les en-têtes HTTP les plus courants sont :

* **Content-Type** : le type de contenu de la réponse, par exemple "application/json" ou "text/html"
* **Content-Length** : la taille du contenu de la réponse, par exemple "1234"
* **Cache-Control** : les directives de cache pour la réponse, par exemple "no-cache"
* **Location** : l'URL de la nouvelle ressource créée, par exemple "/articles/123"


### Body

Les réponses API REST peuvent également inclure un corps de réponse, qui contient les données renvoyées par la requête.
Le corps de la réponse peut être au format JSON, XML ou d'autres formats spécifiques. Le format le plus couramment
utilisé est le **format JSON**.

<u>Exemple de réponse JSON d'une liste d'articles :</u>

```json
[
  {
    "id": 123,
    "title": "My first article",
    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing"
  },
  {
    "id": 456,
    "title": "My second article",
    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing"
  }
]
```

## L'authentification dans les API REST

Il existe plusieurs types d'authentification dans les API REST, voici quelques exemples :

**Basic Authentication** : C'est l'un des types d'authentification les plus simples et courants utilisés dans les 
API REST. Il utilise un nom d'utilisateur et un mot de passe pour accéder à l'API, qui sont encodés en base64 et 
inclus dans l'en-tête de la requête.

Exemple de headers  :
```json
{
  "Authorization" : "Basic am9obkBleGFtcGxlLmNvbTphYmMxMjM="
}
```

**Bearer Authentication** : C'est un type d'authentification basé sur les jetons qui est souvent utilisé dans 
les applications web et mobiles. Le client envoie un jeton d'authentification (généralement JWT - JSON Web Token) 
dans l'en-tête d'autorisation de la requête, qui est vérifié par le serveur pour autoriser l'accès à l'API 
[en savoir plus](https://jwt.io/). Le jeton JWT offre des mesures de sécurité supplémentaires :

* **durée de validité**,  après quoi le client doit se réauthentifier pour obtenir un nouveau jeton d'accès. 
* **une clé de signature** qui permet de vérifier que le jeton a été créé par le serveur et non par un attaquant.


```json
{
  "Authorization" : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
}
```


**OAuth 2.0** : C'est un protocole d'authentification et d'autorisation largement utilisé pour les API REST. 
Il permet aux utilisateurs d'autoriser les applications tierces à accéder à leurs données sans avoir à partager 
leur mot de passe. OAuth 2.0 utilise des jetons d'accès pour autoriser l'accès aux ressources de l'utilisateur.


## Mise en pratique avec Postman

Postman est un outil de développement API REST qui permet de tester les API REST en envoyant des requêtes HTTP.

Lien vers le site officiel : [https://www.postman.com/](https://www.postman.com/)

![postman.png](images%2Fpostman.png)






