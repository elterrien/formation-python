import requests


class AddressApi:

    def __init__(self):
        self.base_url = "https://api-adresse.data.gouv.fr/"

    def _request_api(self, method, url, **kwargs):
        try:
            response = requests.request(method, url, **kwargs)
            if response.ok:
                return response.json()
            else:
                return None
        except requests.exceptions.RequestException as e:
            return None

    def search_by_address(self, address: str):
        payload = {"q": address.replace(" ", "+")}
        return self._request_api("GET", f"{self.base_url}search/", params=payload)

    def search_by_coordinates(self, lat: float, lon: float):
        payload = {"lat": lat, "lon": lon}
        return self._request_api("GET", f"{self.base_url}search/", params=payload)